// extor-static-cookie is a server pluggable transport that presents an external
// Extended ORPort (ExtORPort) using a static authentication cookie file, and
// copies the ExtORPort metadata to its own managing process (using the managing
// process's own authentication cookie file). Use it when you have an external
// non-managed server pluggable transport that wants to provide ExtORPort
// information, but does not have access to the managing process's
// authentication cookie file, or does not know which cookie file to use.
//
// The program takes one command-line argument, the path to an authentication
// cookie file. Other configuration information (e.g. what address to listen on,
// the managing process's ExtORPort address, the path to the managing process's
// authentication cookie file) are taken from pluggable transport environment
// variables.
//
// To configure in torrc, do something like:
//
//	ExtORPort auto
//	ServerTransportPlugin extor_static_cookie exec /usr/local/bin/extor-static-cookie /var/lib/static_extended_orport_auth_cookie
//	ServerTransportListenAddr extor_static_cookie 127.0.0.1:10001
//
// # Options
//
// The "orport-srcaddr" option specifies an IP address (e.g. "127.0.0.2") or
// range of IP addresses (e.g. "127.0.2.0/24") to use when dialing the upstream
// ExtORPort. This can be useful to avoid running out of localhost ephemeral
// ports.
//
//	ServerTransportOptions extor_static_cookie orport-srcaddr=127.0.2.0/24
package main

import (
	"errors"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"net"
	"os"
	"os/signal"
	"sync"
	"syscall"

	pt "git.torproject.org/pluggable-transports/goptlib.git"
)

func main() {
	flag.Usage = func() {
		fmt.Fprintf(flag.CommandLine.Output(), `Usage: %s [COOKIE_FILENAME]

This program is meant to be run as a managed pluggable transport.
Try something like this in a torrc file:
	ExtORPort auto
	ServerTransportPlugin extor_static_cookie exec /usr/local/bin/extor-static-cookie /var/lib/static_extended_orport_auth_cookie
	ServerTransportListenAddr extor_static_cookie 127.0.0.1:10001
`, os.Args[0])
		flag.PrintDefaults()
	}
	flag.Parse()

	ptInfo, err := pt.ServerSetup(nil)
	if err != nil {
		// pt.ServerSetup prints err.
		os.Exit(1)
	}

	var initErr error
	if flag.NArg() != 1 {
		flag.Usage()
		pt.Log(pt.LogSeverityError, fmt.Sprintf("need one command-line argument"))
		os.Exit(1)
	}
	cookie, err := readAuthCookieFile(flag.Arg(0))
	if err != nil {
		err := fmt.Errorf("read %v: %w", flag.Arg(0), err)
		// Log the error here, and also remember it to report in
		// SMETHOD-ERROR.
		pt.Log(pt.LogSeverityError, err.Error())
		if initErr == nil {
			initErr = err
		}
	}

	listeners := make([]*net.TCPListener, 0)
	var wg sync.WaitGroup
	for _, bindaddr := range ptInfo.Bindaddrs {
		// If there was an initialization error, report it for each
		// requested bindaddr, as well as logging it above.
		if initErr != nil {
			pt.SmethodError(bindaddr.MethodName, initErr.Error())
			continue
		}

		// Ignore bindaddr.MethodName. "extor_static_cookie" is used in
		// examples, but we accept anything.

		// Are we requested to use source addresses from a particular
		// range when dialing the ORPort for this transport?
		var srcAddr *net.IPNet
		if srcAddrCIDR, ok := bindaddr.Options.Get("orport-srcaddr"); ok {
			ipnet, err := parseIPCIDR(srcAddrCIDR)
			if err != nil {
				err = fmt.Errorf("parsing orport-srcaddr: %w", err)
				pt.SmethodError(bindaddr.MethodName, err.Error())
				continue
			}
			srcAddr = ipnet
		}

		ln, err := net.ListenTCP("tcp", bindaddr.Addr)
		if err != nil {
			pt.SmethodError(bindaddr.MethodName, err.Error())
			continue
		}
		pt.Smethod(bindaddr.MethodName, ln.Addr())
		listeners = append(listeners, ln)
		wg.Add(1)
		bindaddr := bindaddr
		go func() {
			defer wg.Done()
			defer ln.Close()
			err := acceptLoop(ln, &ptInfo, cookie, srcAddr)
			if err != nil {
				pt.Log(pt.LogSeverityError, fmt.Sprintf(
					"acceptLoop: %v at %v: terminated with error: %v",
					bindaddr.MethodName, bindaddr.Addr, err))
			}
		}()
	}
	pt.SmethodsDone()

	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, syscall.SIGTERM)

	if os.Getenv("TOR_PT_EXIT_ON_STDIN_CLOSE") == "1" {
		// This environment variable means we should treat EOF on stdin
		// just like SIGTERM: https://bugs.torproject.org/15435.
		go func() {
			io.Copy(ioutil.Discard, os.Stdin)
			sigChan <- syscall.SIGTERM
		}()
	}

	// Terminate the main program if all listeners have terminated.
	go func() {
		wg.Wait()
		sigChan <- syscall.SIGTERM
	}()

	// Wait for a reason to terminate.
	<-sigChan
	for _, ln := range listeners {
		ln.Close()
	}
}

// acceptLoop accepts connections from ln, and passes each one to handler along
// with ptInfo.
func acceptLoop(ln *net.TCPListener, ptInfo *pt.ServerInfo, cookie authCookie, srcAddr *net.IPNet) error {
	for {
		conn, err := ln.AcceptTCP()
		if err != nil {
			if e, ok := err.(net.Error); ok && e.Temporary() {
				continue
			}
			return err
		}
		go func() {
			defer conn.Close()
			err := handler(conn, ptInfo, cookie, srcAddr)
			if err != nil {
				pt.Log(pt.LogSeverityError, fmt.Sprintf("handler: %v", err))
			}
		}()
	}
}

// handler does an ExtORPort negotiation with the client conn, then dials the
// ExtORPort of its own pluggable transport manager, copying the metadata from
// the incoming client connection. It then proxies the remainder of the
// connection.
//
// If srcAddr is not nil, then a random IP address in the given network is used
// as the source address when dialing the ExtORPort.
func handler(conn *net.TCPConn, ptInfo *pt.ServerInfo, cookie authCookie, srcAddr *net.IPNet) error {
	metadata, err := extORServerHandshake(conn, cookie)
	if err != nil {
		return err
	}

	dialer := net.Dialer{
		Control: dialerControl,
	}
	if srcAddr != nil {
		// Use a random source IP address in the given range.
		ip, err := randIPAddr(srcAddr)
		if err != nil {
			return err
		}
		dialer.LocalAddr = &net.TCPAddr{IP: ip}
	}
	or, err := pt.DialOrWithDialer(&dialer, ptInfo, metadata.Useraddr, metadata.Transport)
	if err != nil {
		return err
	}
	defer or.Close()

	return proxy(conn, or.(*net.TCPConn))
}

// proxy copies from a to b and from b to a, persisting until both copies have
// finished, and returns the first error encountered that is not
// io.ErrClosedPipe.
func proxy(a, b *net.TCPConn) error {
	// Each goroutine sends once on errChan when it finishes.
	errChan := make(chan error, 2)
	go func() {
		_, err := io.Copy(b, a)
		errChan <- err
		a.CloseRead()
		b.CloseWrite()
	}()
	go func() {
		_, err := io.Copy(a, b)
		errChan <- err
		b.CloseRead()
		a.CloseWrite()
	}()

	var err error
	if err2 := <-errChan; err == nil && err2 != nil && !errors.Is(err2, io.ErrClosedPipe) {
		err = err2
	}
	if err2 := <-errChan; err == nil && err2 != nil && !errors.Is(err2, io.ErrClosedPipe) {
		err = err2
	}
	return err
}
