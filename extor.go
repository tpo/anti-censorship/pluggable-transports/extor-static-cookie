package main

import (
	"bytes"
	"crypto/hmac"
	"crypto/rand"
	"crypto/sha256"
	"fmt"
	"io"
	"io/ioutil"
	"os"
)

// authCookie represents a 32-octet authentication cookie.
//
// https://gitweb.torproject.org/torspec.git/tree/ext-orport-spec.txt?id=29245fd50d1ee3d96cca52154da4d888f34fedea#n81
type authCookie [32]byte

// readAuthCookie reads an authentication cookie from a Reader.
func readAuthCookie(r io.Reader) (authCookie, error) {
	var cookie authCookie
	// Read 1 byte beyond the expected file length, in order to ensure that
	// the file really ends there.
	data, err := ioutil.ReadAll(io.LimitReader(r, 65))
	if err != nil {
		return cookie, err
	}
	if len(data) != 64 || !bytes.Equal(data[0:32], []byte("! Extended ORPort Auth Cookie !\n")) {
		return cookie, fmt.Errorf("bad file format")
	}
	copy(cookie[:], data[32:64])
	return cookie, nil
}

// readAuthCookieFile reads an authentication cookie from a named file.
func readAuthCookieFile(path string) (cookie authCookie, err error) {
	var f *os.File
	f, err = os.Open(path)
	if err != nil {
		return
	}
	defer func() {
		err2 := f.Close()
		if err == nil {
			err = err2
		}
	}()
	return readAuthCookie(f)
}

// extORMetadata represents the metadata associated with an ExtORPort
// connection.
type extORMetadata struct {
	Useraddr  string
	Transport string
}

// extORServerHandshake carries out ExtORPort authentication and command
// exchange with a client, returning the connection metadata derived from the
// client's command, and leaving the connection in a state to exchange raw data.
func extORServerHandshake(c io.ReadWriter, cookie authCookie) (*extORMetadata, error) {
	// First, authenticate the client.
	err := extORAuthenticateClient(c, cookie)
	if err != nil {
		return nil, err
	}

	// Read the client's ExtORPort commands.
	// https://gitweb.torproject.org/torspec.git/tree/ext-orport-spec.txt?id=29245fd50d1ee3d96cca52154da4d888f34fedea#n145
	const (
		commandDone      = 0x0000
		commandUseraddr  = 0x0001
		commandTransport = 0x0002
	)
	var metadata extORMetadata
	for done := false; !done; {
		command, body, err := extORReadCommand(c)
		if err != nil {
			return nil, err
		}
		switch command {
		case commandDone:
			done = true
		// We do not attempt to validate the syntax of command bodies
		// (IP address format of USERADDR or C identifier–ness of
		// TRANSPORT); let the managing process do that. If a command is
		// sent multiple times, the last one wins.
		case commandUseraddr:
			metadata.Useraddr = string(body)
		case commandTransport:
			metadata.Transport = string(body)
		}
	}
	// Finally, send the single command COMMAND=OKAY, BODYLEN=0.
	_, err = c.Write([]byte{0x10, 0x00, 0x00, 0x00})
	if err != nil {
		return nil, err
	}
	return &metadata, nil
}

// extORAuthenticateClient carries out the ExtORPort authentication protocol
// with the client c. When a nil error is returned, it means that client
// authentication was successful and the connection is ready to begin the
// ExtORPort protocol. When a non-nil error is returned, it means that client
// authentication failed, or that there was an I/O error.
func extORAuthenticateClient(c io.ReadWriter, cookie authCookie) error {
	// https://gitweb.torproject.org/torspec.git/tree/ext-orport-spec.txt?id=29245fd50d1ee3d96cca52154da4d888f34fedea#n32
	const (
		endAuthTypes       = 0
		authTypeSafeCookie = 1
	)
	// When a client connects to the Extended ORPort, the server sends:
	//   AuthTypes    [variable]
	//   EndAuthTypes [1 octet]
	_, err := c.Write([]byte{authTypeSafeCookie, endAuthTypes})
	if err != nil {
		return err
	}
	// The client reads the list of supported authentication schemes and
	// replies with the one he prefers to use:
	//   AuthType     [1 octet]
	var authType [1]byte
	_, err = io.ReadFull(c, authType[:])
	if err != nil {
		return err
	}
	if authType[0] != authTypeSafeCookie {
		return fmt.Errorf("client offered unsupported AuthType %d", authType[0])
	}

	// A client that performs the SAFE_COOKIE handshake begins by sending:
	//   ClientNonce  [32 octets]
	var clientNonce [32]byte
	_, err = io.ReadFull(c, clientNonce[:])
	if err != nil {
		return err
	}
	// ServerNonce is 32 random octets.
	var serverNonce [32]byte
	_, err = io.ReadFull(rand.Reader, serverNonce[:])
	if err != nil {
		return err
	}
	// ServerHash is computed as:
	//   HMAC-SHA256(CookieString, "ExtORPort authentication server-to-client hash" | ClientNonce | ServerNonce)
	var serverHash [32]byte
	{
		h := hmac.New(sha256.New, cookie[:])
		h.Write([]byte("ExtORPort authentication server-to-client hash"))
		h.Write(clientNonce[:])
		h.Write(serverNonce[:])
		copy(serverHash[:], h.Sum(nil))
	}
	// Then, the server replies with:
	//   ServerHash   [32 octets]
	//   ServerNonce  [32 octets]
	_, err = c.Write(serverHash[:])
	if err != nil {
		return err
	}
	_, err = c.Write(serverNonce[:])
	if err != nil {
		return err
	}
	// ClientHash is computed as:
	//   HMAC-SHA256(CookieString, "ExtORPort authentication client-to-server hash" | ClientNonce | ServerNonce)
	var expectedClientHash [32]byte
	{
		h := hmac.New(sha256.New, cookie[:])
		h.Write([]byte("ExtORPort authentication client-to-server hash"))
		h.Write(clientNonce[:])
		h.Write(serverNonce[:])
		copy(expectedClientHash[:], h.Sum(nil))
	}
	// If the server-provided ServerHash is invalid, the client MUST
	// terminate the connection. Otherwise the client replies with:
	//   ClientHash   [32 octets]
	var clientHash [32]byte
	_, err = io.ReadFull(c, clientHash[:])
	if err != nil {
		return err
	}
	// Finally, the server replies with:
	//   Status       [1 octet]
	if hmac.Equal(clientHash[:], expectedClientHash[:]) {
		_, err = c.Write([]byte{1})
	} else {
		_, err = c.Write([]byte{0})
		if err == nil {
			err = fmt.Errorf("ClientHash mismatch")
		}
	}
	return err
}

// extORReadCommand reads and returns an ExtORPort command and the associated
// body.
func extORReadCommand(c io.Reader) (uint16, []byte, error) {
	// https://gitweb.torproject.org/torspec.git/tree/ext-orport-spec.txt?id=29245fd50d1ee3d96cca52154da4d888f34fedea#n145
	var buf [4]byte
	_, err := io.ReadFull(c, buf[:])
	if err != nil {
		return 0, nil, err
	}
	command := (uint16(buf[0]) << 8) | uint16(buf[1])
	bodyLen := (uint16(buf[2]) << 8) | uint16(buf[3])
	body := make([]byte, bodyLen)
	_, err = io.ReadFull(c, body)
	if err != nil {
		return 0, nil, err
	}
	return command, body, nil
}
