module gitlab.torproject.org/dcf/extor-static-cookie

go 1.15

require (
	git.torproject.org/pluggable-transports/goptlib.git v1.3.0
	golang.org/x/sys v0.8.0
)
