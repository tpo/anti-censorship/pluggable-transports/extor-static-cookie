package main

import (
	"bytes"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"testing"
)

func TestReadAuthCookie(t *testing.T) {
	// Bad format.
	for _, data := range []string{
		"",
		"foobar",
		"! Extended ORPort Auth Cookie !\n",
		"! Extended ORPort Auth Cookie !\n0123456789abcdef0123456789abcde",   // too short
		"! Extended ORPort Auth Cookie !\n0123456789abcdef0123456789abcdefX", // too long
	} {
		_, err := readAuthCookie(strings.NewReader(data))
		if err == nil {
			t.Errorf("%+q resulted in no error", data)
			continue
		}
	}
	// Good format.
	for _, data := range []string{
		"! Extended ORPort Auth Cookie !\n0123456789abcdef0123456789abcdef",
		"! Extended ORPort Auth Cookie !\n0123456789abcdef0123456789abcde\n",
		"! Extended ORPort Auth Cookie !\n\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00",
	} {
		cookie, err := readAuthCookie(strings.NewReader(data))
		if err != nil {
			t.Errorf("%+q resulted in error %v", data, err)
		} else if !bytes.Equal(cookie[:], []byte(data)[32:64]) {
			t.Errorf("%+q cookie %+q != %+q", data, cookie, data[32:64])
		}
	}
}

func TestReadAuthCookieFile(t *testing.T) {
	// Error expected.
	{
		dir, err := ioutil.TempDir("", "extor-static-cookie_test_")
		if err != nil {
			panic(err)
		}
		// Reading a directory as a file.
		_, err = readAuthCookieFile(dir)
		if err == nil {
			t.Errorf("reading from directory resulted in no error")
		}
		// Reading a nonexistent file. (Nonexistent because it is under
		// the empty directory we just created.)
		_, err = readAuthCookieFile(filepath.Join(dir, "extended_orport_auth_cookie"))
		if err == nil {
			t.Errorf("reading from nonexistent file resulted in no error")
		}
		if err := os.Remove(dir); err != nil {
			panic(err)
		}
	}
	// No error expected.
	for _, test := range []struct {
		path   string
		cookie []byte
	}{
		{"test_auth_cookie", []byte("29da9455048c6a2e07ee4a9b844d237\n")},
	} {
		cookie, err := readAuthCookieFile(test.path)
		if err != nil {
			t.Errorf("file %s resulted in error %v", test.path, err)
		} else if !bytes.Equal(cookie[:], test.cookie) {
			t.Errorf("file %s cookie was %+q", test.path, cookie)
		}
	}
}
